============
Installation
============

Check Prerequisites
-------------------

Before installing, ensure that you have the following:

* Google account
* JAVA Development Kit (JDK)
    * Supported Versions
        +---------------+---------------------+
        | Vendor        | Version             |
        +===============+=====================+
        | OpenJDK       | 8                   |
        +---------------+---------------------+
        | Oracle JDK    | 8                   |
        +---------------+---------------------+
* Web Application Container
    +---------------+---------------------+
    | Web Container | Version             |
    +===============+=====================+
    | Apache Tomcat | 8.5                 |
    +---------------+---------------------+
* User Store
    * Supported User Stores
        +----------------------------+---------------------+
        | Vendor                     | Version             |
        +============================+=====================+
        | Microsoft Active Directory | 2012, 2016          |
        +----------------------------+---------------------+
        | Wren:DS server             | 3.0                 |
        +----------------------------+---------------------+
        | OpenLDAP server            | 2.4                 |
        +----------------------------+---------------------+
        | 389 Directory server       | 1.4                 |
        +----------------------------+---------------------+
        | MySQL Database server      | 5.7                 |
        +----------------------------+---------------------+
        | MariaDB Database server    | 10.3, 10.4          |
        +----------------------------+---------------------+

    This guide assumes basic knowledge of tomcat and the databases used.


Register recaptcha key
----------------------

.. figure:: ../_static/images/create-recaptcha-sitekey.gif
    :align: center
    :figwidth: 600px
    :target: ../_static/images/create-recaptcha-sitekey.gif

#. `Go to google's recaptcha creation page <https://www.google.com/recaptcha/admin/create>`_

#. Fill in the form
    +----------------+---------------------------+----------------------+
    | Field          | Value                     | Meaning              |
    +================+===========================+======================+
    | Label          | CIAM                      | the name for you to  |
    |                |                           | identify the site by |
    +----------------+---------------------------+----------------------+
    | ReCAPTCHA type | Invisible reCAPTCHA Badge | what kind of         |
    |                |                           | challenge users will |
    |                |                           | receive when using   |
    |                |                           | your site            |
    +----------------+---------------------------+----------------------+
    | Domains        | e.g. localhost,           | the domain name of   |
    |                | your-domain-name.com      | the site that will be|
    |                |                           | protected (including |
    |                |                           | subdomains)          |
    +----------------+---------------------------+----------------------+
    | Owners         | *leave blank*             | people who will have |
    |                |                           | access to these      |
    |                |                           | settings             |
    +----------------+---------------------------+----------------------+

#. Click save

.. NOTE::
    Keep this window open, you will need the sitekey soon


Deploy the WAR file to Tomcat
-----------------------------

Deploy the provided WAR file to the installed Tomcat. Make sure the web application is exploded into Tomcat's webapp folder.


Enter site-key into the actual html file for forgot password
------------------------------------------------------------

.. NOTE::

    There are plans to simplify the recaptcha set up process.

    You can follow the `the latest progress here <https://gitlab.com/identiticoders/ciam/issues/5>`_

#. Open the file at **<tomcat_folder>/web-apps/ciam/forget-password.html**
#. Look for the element with the id "reset-password-button"
#. Change its data-sitekey attribute to the sitekey shown in step 3.


Enter connection parameters for Database and User Store into the Properties file
--------------------------------------------------------------------------------

#. Open the file at **<tomcat_folder>/web-apps/ciam/WEB-INF/classes/application.properties**
#. Update the parameters for Database, SMTP Server and API Gateway accordingly


Start Tomcat and Test
---------------------

Start tomcat and access the URL http://<server_ip>:<port>/ciam/forget-password.html