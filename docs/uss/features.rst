Features
========

Some of the most notable features in User Self-Service are discussed in this section.

Google reCaptcha
----------------

To avoid automated form submissions and spam, reCAPTCHA from Google, is implemented to attest users if they are human, and not a robot. The Google reCAPTCHA takes-away most of challenges enterprises face in customer on-boarding and provides the right balance between usability and security.

.. figure:: ../_static/images/uss-recaptcha.png
    :align: center
    :figwidth: 600px
    :target: ../_static/images/uss-recaptcha.png