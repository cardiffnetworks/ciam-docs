Customizations
==============

The purpose of this section is to provide administrators with documentation to customize the IC.SG CIAM User Self-Service (USS).

Branding
--------

It is possible to change the default logo in USS.

.. figure:: ../_static/images/uss-forget-password.png
    :align: center
    :figwidth: 600px
    :target: ../_static/images/uss-forget-password.png


Steps:

- Prepare a logo file.
- The file name must be **icon-full.png**, no more than 50kB, with dimensions 380px x 480px exactly.
- Upload logo file to webapp/ciam/assets



Favicon
-------

A favicon (short for favorite icon), also known as a shortcut icon, website icon, tab icon, URL icon, or bookmark icon, is a file containing one or more small icons, associated with a particular website or web page. It is possible to change the favicon in USS.

Steps:

- Prepare a favicon file.
- The file name must be **favicon.ico**, no more than 50kB, with dimensions 64px x 64px exactly.
- Upload favicon file to webapp/ciam/assets

