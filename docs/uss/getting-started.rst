Introduction
============

The purpose of this document is to provide the changes made/new features included in this release of the IC.SG CIAM User Self-Service.

Background
----------

User Self-Service is self-service portal for users who require forget password and password reset services. It is part of the offering from the next-generation Customer Identity & Access Management (CIAM) Dashboard designed users to access protected applications in a single page application.


Architecture
------------

.. figure:: ../_static/images/uss-architecture.png
    :align: center
    :figwidth: 464px
    :target: ../_static/images/uss-architecture.png

User Self-Service (USS) is built to integrate with Microsoft Active Directory, any LDAPv3-compliant server and any database server. In addition, for ease of integration, USS has ready-built interfaces to integrate with any API Gateway.
