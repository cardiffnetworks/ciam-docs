Customer Identity & Access Management (CIAM)
============================================

CIAM Dashboard is dashboard for Customer Identity and Access Management platform. It is usually integrated with a Single Sign-On (SSO) solutions where applications are protected. The landing portal displays notifications and short-cuts to all protected applications. There is also an analytics page for administrators to monitor statistics like popular applications by departments, total logins by applications, frequent access times for a particular application and login failure rate for a particular application etc.

.. image:: ./_static/images/ciam-0.png


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: User Self-Service

   uss/getting-started
   uss/installation
   uss/features
   uss/customizations
    
.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: About

   authors
   support


