Support & License
=================

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: jd@ic.sg

License
-------

**Attribution-NoDerivs CC BY-ND**

This documentation is released under a CC BY-ND license.  

The documentation is written by experts from IC.SG who work very hard for the successful implementation of Customer Identity and Access Management (CIAM) platform and User Self-Service (USS).

This license lets you reuse the documentation for any purpose, including commercially; however, it cannot be shared with others in adapted form, and credit must be provided to us.


.. image:: _static/images/2880px-Cc-by-nd_icon.svg.png 
